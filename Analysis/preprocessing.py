#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 27 10:05:51 2022

@author: olegol

This script adds N and renames/reorders columns, output ready to use in
munge
"""

import os
import pandas as pd
pd.options.mode.chained_assignment = None  # default='warn'
import time
import numpy as np

# set paths to summary statistics and output folder
path2files = '/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data/merged'
path2output = '/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data/processed'


files = [os.path.join(path2files, s) for s in os.listdir(path2files)]
traits = os.listdir(path2files)

N = 26530
sanity_check = []

start_time = time.time()

for x in range(len(files)):
    df = pd.read_csv(files[x], sep='\t', header=0, low_memory=False)
    result_df = df.drop_duplicates(keep=False)
    result_df['LOG10P'] = pd.to_numeric(result_df['LOG10P'])
    result_df['LOG10P'] = 10 ** (-result_df['LOG10P'])
    result_df['N'] = N
    result_df.rename(
        columns={'CHROM': 'CHR', 'GENPOS': 'BP', 'ID': 'SNP', 'ALLELE0': 'A2', 'ALLELE1': 'A1', 'LOG10P': 'P'},
        inplace=True)
    result_df = result_df.reindex(columns=['SNP', 'A1', 'A2', 'BETA', 'SE', 'P', 'N', 'CHR', 'BP'])
    result_df.to_csv(os.path.join(path2output, traits[x]), sep=" ", index=None)
    sanity_check.append(len(df))

print("Running the script lasted %s minutes" % ((time.time() - start_time)/60))
print('Sanity check: Max length of dataset =', max(sanity_check),
      ', Min length of dataset =', min(sanity_check))
