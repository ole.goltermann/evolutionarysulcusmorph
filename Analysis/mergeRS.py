#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Feb 07

@author: olegol

Script to merge RS numbers using left join in pandas
"""

import pandas as pd
import os

path2files ='/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data/test/Rsnumber_test/SNP_151/readytoimplement'


df_1 = pd.read_csv(os.path.join(path2files, 'SNP_rs_DUP.tsv'), sep=' ', header=0)
df_2 = pd.read_csv(os.path.join(path2files, 'w_hm3.tsv'),sep='\t', header=0)
df_3 = pd.merge(df_1, df_2, on = ['SNP'])
print(len(df_3))

start_time = time.time()

df1 = pd.read_csv(os.path.join(path2files, 'SNP_nodup.tsv'), sep=' ', header=None)
df1.rename(columns={0: 'CHR', 1: 'BP', 2: 'RS'}, inplace=True)
df2 = pd.read_csv(os.path.join(path2files, 'example-COMB_disEUR.tsv.gz'), sep=' ', header=0)

df3 = pd.merge(df1, df2, on = ['CHR', 'BP'], how = 'outer')
df3.rename(columns={'SNP': 'old_ID', 'RS': 'SNP'}, inplace=True)
df4 = df3.fillna('NA')
 
df4.to_csv(os.path.join(path2files, 'test.tsv.gz'), sep=" ", index=None)


df_stats = pd.read_csv('/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data/test/example-COMB_disEUR.tsv.gz', sep=' ', header=0)
df_stats.SNP.fillna(df_stats.old_ID, inplace=True)
df4.to_csv(os.path.join(path2files, 'test.tsv.gz'), sep=" ", index=None)



print("--- %s seconds ---" % (time.time() - start_time))
