#!/bin/sh
# @author: ole.goltermann@maxplanckschools.de
#---------------------------------------------------------------------------------------------------
#set up paths
#---------------------------------------------------------------------------------------------------
working_folder=/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data
rs_files=$working_folder/rs_files
processed_sumstats=$working_folder/processed
output_len=$working_folder/len_processed

echo $rs_files
echo $processed_sumstats
echo $output_len

#---------------------------------------------------------------------------------------------------
# load in processed_sumstats $ rs_file and copy rs number 
#---------------------------------------------------------------------------------------------------


for sumstats in ${processed_sumstats}/*gz; do
	filename=$(basename ${sumstats})
	pheno=${filename%-COMB*gz}
	if [ ! -f ${output_len}/processed_${filename} ]; then
		echo 'Find rsID for:' ${pheno}
		# unzip and save as temporary file (step is necessary because shell cannot deal with zcat in awk command)
		zcat $sumstats > temp1.tsv
		# save merging file to temporary stored one
		awk 'NR==FNR{ seen[$1FS$2]=$3; next } { print $0, seen[$8FS$9] }' $rs_files/SNP_all_nodup.tsv temp1.tsv > temp2.tsv
		# fill missings with old ID, change column names and reorder columns
		awk 'NR==FNR{max=(NF>max)?NF:max;next}{for(i=0;++i<=max;)$i=($i)?$i:$1}1' temp2.tsv temp2.tsv | 
		sed -e "1s/.*/old_ID A1 A2 BETA SE P N CHR BP SNP/" | 
		awk '{ print $10,$2,$3,$4,$5,$6,$7,$8,$9,$1 }' |
		# zip and save
		gzip > $output_len/processed_${filename}
		# remove temp files
		rm temp1.tsv
		rm temp2.tsv
	fi
done