#!/bin/sh
# @author: ole.goltermann@maxplanckschools.de
#---------------------------------------------------------------------------------------------------
#set up paths
#---------------------------------------------------------------------------------------------------
working_folder=/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data
rs_files=$working_folder/rs_files
processed_sumstats=$working_folder/processed_lux19
output_ukbb=$working_folder/ukbb_processed

echo $rs_files
echo $processed_sumstats
echo $output_len

#---------------------------------------------------------------------------------------------------
# load in processed_sumstats $ rs_file and copy rs number 
#---------------------------------------------------------------------------------------------------


for sumstats in ${processed_sumstats}/*gz; do
	filename=$(basename ${sumstats})
	pheno=${filename%-COMB*gz}
	if [ ! -f ${output_ukbb}/processed_${filename} ]; then
		echo 'Find rsID for:' ${pheno}
		# unzip and save as temporary file (step is necessary because shell cannot deal with zcat in awk command)
		zcat $sumstats > temp1.tsv
		# get all sumstats originally based on GRCh37, and paste new columns to match with IDs 
		grep 'imp' temp1.tsv > temp1_37.tsv
		awk '{print $1}' temp1_37.tsv | cut -f1,2 -d':' > temp_imp37.tsv
		paste -d ' ' temp1_37.tsv temp_imp37.tsv > temp37.tsv
		# get all sumstats based on GRCh38
		grep -v 'imp' temp1.tsv > temp38.tsv
		# match with RS files and copy rsIDS
		awk 'NR==FNR{ seen[$2FS$3FS$4]=$1; next } { print $0, seen[$10FS$2FS$3] }' $rs_files/SNP_rsonlyUKBB.tsv temp37.tsv > temp_37.tsv
		awk 'NR==FNR{ seen[$1FS$2FS$3FS$4]=$5; next } { print $0, seen[$8FS$9FS$3FS$2] }' $rs_files/SNP_rsUKBB38_nodup.tsv temp38.tsv > temp_38.tsv
		# rearrange both files and prepare them to combine
		awk 'NR==FNR{max=(NF>max)?NF:max;next}{for(i=0;++i<=max;)$i=($i)?$i:$1}1' temp_37.tsv temp_37.tsv | awk '{ print $11,$2,$3,$4,$5,$6,$7,$8,$9,$1 }' > temp2_37.tsv
		awk 'NR==FNR{max=(NF>max)?NF:max;next}{for(i=0;++i<=max;)$i=($i)?$i:$1}1' temp_38.tsv temp_38.tsv | sed -e "1s/.*/old_ID A1 A2 BETA SE P N CHR BP SNP/" | awk '{ print $10,$2,$3,$4,$5,$6,$7,$8,$9,$1 }' > temp2_38.tsv
		# combine both files and sort them 
		cat temp2_38.tsv temp2_37.tsv | sort -k8n -k9n |
		# zip it
		gzip > $output_ukbb/processed_${filename}
		# remove temp files
		rm temp1.tsv
		rm temp1_37.tsv
		rm temp_37.tsv
		rm temp_38.tsv
		rm temp_imp37.tsv
		rm temp37.tsv
		rm temp38.tsv
		rm temp2_37.tsv
		rm temp2_38.tsv
	fi
done