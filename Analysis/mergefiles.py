#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 26 10:05:51 2022

@author: olegol

This script merges the 22 tsv.gz chromosome files per trait
"""

import re
import os
import time

# set paths to summary statistics and output folder
path2files = '/data/workspaces/lag/workspaces/lg-sulcal-morphology/primary_data/sumstats/'
path2output = '/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data/merged'
fileend = '-COMB_disEUR.tsv.gz'

# Get list of all individual traits
pattern = '-\d+'
list_files = os.listdir(path2files)
list_traits = [re.split(pattern,x)[0] for x in list_files]
list_tunique = list(set(list_traits))

print('Number of files in the folder: ', len(list_files))
print('Number of individual traits: ', len(list_tunique))


# Get list of all chromosomes
list_chrom = [int(re.findall(pattern,x)[0]) for x in list_files]
list_cunique = list(set(list_chrom))

print('Number of chromosomes per trait: ', len(list_cunique))


# Merge chromosome summary stats for individual traits

start_time = time.time()

for x in range(len(list_tunique)):
    trait_list = [m for m in list_files if list_tunique[x] in m]
    trait_list.sort(key=lambda x: int(''.join(filter(str.isdigit, x))))
    with open(os.path.join(path2output,list_tunique[x]+fileend), 'ab') as result:  # append in binary mode
        for f in [path2files + s for s in trait_list]:
            with open(f, 'rb') as tmpf:        # open in binary mode
                result.write(tmpf.read())
                
print("--- %s seconds ---" % (time.time() - start_time))
