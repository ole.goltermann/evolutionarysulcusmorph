#!/bin/sh

#---------------------------------------------------------------------------------------------------
#set up our files: the general input path and the outputfolder
#and munged sumstats
#---------------------------------------------------------------------------------------------------

#output gen cor
out_path_gencor=/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data/gencor_phenoSPD_exp


# ---------------------------------------------------------------------------------------------------
#extract gen cor resuls 
#----------------------------------------------------------------------------------------------------
#little wrapper to extract the summary values from the LDSC log after running gen cor
echo "trait rg se z p h2_obs h2_obs_se h2_int h2_int_se gcov_int gcov_int_se"> $out_path_gencor/gencor_sulci_phenoSPD_exp.txt
for i in $out_path_gencor/*log; do
  res=$(cat $i | grep '^/d.*sumstat' | cut -d" " -f4-)
  pheno=$(basename "$i")
  echo $pheno $res >> $out_path_gencor/gencor_sulci_phenoSPD_exp.txt
done 