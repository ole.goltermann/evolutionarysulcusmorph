#--------------------------
#---- Clumping w/PLINK ----
#--------------------------

# Gokberk Alagoz, 2021 - last updated March 2022
# Clump sumstats before the overlap analysis.

#-----Variables-----

#TODO 1- Make a list of your sumstats (or change this script to loop over them)
#TODO 2- Update paths to sumstats_list.txt and to your output directory.
#TODO 3- ssh to gridmaster, run this script as: bash clump.sh

sumstatsList="/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data/overlap/list_sumstats.txt"
outDir="/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data/overlap/"
genotypeFile="/data/workspaces/lag/shared_spaces/Resource_DB/1KG_phase3/GRCh37/plink/1KG_phase3_GRCh37_EUR_nonFIN_allchr.bim"

#-----

echo "Starting to clump..."
while read i; do
​
	echo "$i"
	#base_name=$(basename "$i")

done < $sumstatsList
echo "Done!"