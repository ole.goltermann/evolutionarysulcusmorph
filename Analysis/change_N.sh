#!/bin/sh
# @author: ole.goltermann@maxplanckschools.de
#---------------------------------------------------------------------------------------------------
#set up paths
#---------------------------------------------------------------------------------------------------
working_folder=/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data
munged_files=$working_folder/munged_N


for sumstats in ${munged_files}/*sumstats; do
	gzip $sumstats
done