grep -E 'rs10935665|rs6794941|rs75563552|rs60465570|rs58177617|rs1500865|rs62275309|rs1500867|rs60852193|rs113270900|rs62275311|rs7639019|rs61115449|rs62275342|rs62275343|rs66507831|rs4411854|rs1909876|rs2133939|rs17567518|rs62275345|rs7629647|rs62275346|rs10513309|rs1394046|rs2651316|rs2651315|rs2654817|rs4578990|rs2654818|rs2651313|rs2654819|rs2654820|rs905575|rs884370' 'processed_usc_width_S.C-left-COMB_disEUR.tsv'
# get 
cat processed_usc_width_S.C-left-COMB_disEUR.tsv | awk '$8 == 3 && $9 >= 147300000 && $9 <= 147400000' > zoom.txt

# merge chr:bp for matching 

awk '{ printf "%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n", $1, $2, $3, $4, $5, $6, $7, "chr"$8":"$9, $8, $9,  $10}' zoom.txt > zoom2.txt


zcat processed_usc_width_F.I.P-left-COMB_disEUR.tsv.gz | awk '$8 == 3 && $9 >= 147300000 && $9 <= 147400000' > zoom_width_F.I.P-left.txt
zcat processed_usc_width_S.Pa.int-left-COMB_disEUR.tsv.gz | awk '$8 == 3 && $9 >= 147300000 && $9 <= 147400000' > zoom_width_S.Pa.int-left.txt
zcat processed_usc_width_S.Pa.int-right-COMB_disEUR.tsv.gz | awk '$8 == 3 && $9 >= 147300000 && $9 <= 147400000' > zoom_width_S.Pa.int-right.txt
zcat processed_usc_width_F.I.P-right-COMB_disEUR.tsv.gz | awk '$8 == 3 && $9 >= 147300000 && $9 <= 147400000' > zoom_width_F.I.P-right.txt