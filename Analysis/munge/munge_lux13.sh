#!/bin/sh
# @author: ole.goltermann@maxplanckschools.de

#set up the python environment needed on lux13 for LDSC
module purge
module load miniconda/3.2021.10 ldsc/v1.0.1
source activate ldsc

#set date 
DATE=`date +"%Y%m%d"`
#---------------------------------------------------------------------------------------------------
#set up our files: input path for sumstats and the outputfolder for munged sumstats
#---------------------------------------------------------------------------------------------------
working_folder=/data/workspaces/lag/workspaces/lg-sulcal-morphology/working_data
sulcal_sumstats=$working_folder/ukbb_processed_lux13
scripts=$working_folder/run_scripts/munge
output_munged=$working_folder/munged

echo $working_folder
echo $scripts
echo $sulcal_sumstats

#---------------------------------------------------------------------------------------------------
#run for loop for munge
#----------------------------------------------------------------------------------------------------


for sumstats in ${sulcal_sumstats}/*gz; do
	filename=$(basename ${sumstats})
	pheno=${filename%-COMB*gz}
	if [ ! -f ${output_munged}/munged_${pheno}.sumstats.gz ]; then
		echo 'run munge on sumstats for' ${pheno}
		munge_sumstats.py \
		--chunksize 500000 \
		--sumstats ${sumstats} \
		--out ${output_munged}/munged_${pheno} \
		--merge-alleles $scripts/w_hm3.snplist
	fi
done


 