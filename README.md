# Evolution of human brain sulcus morphology

`[Last update: 2023-10-24]`


***
Period: January, 2022 - April, 2022 <br>

Project members: Ole Goltermann | Gökberk Alagöz | Barbara Molz | Simon E. Fisher <br>
Where: MPI Psycholingustics, Nijmegen, Netherlands

***


## Briefly
 
In this project we will investigate the relationship between evolutionary annotations (capturing different time scales along the human lineage) and common genetic variants shaping regional sulcal morphology in the human brain.